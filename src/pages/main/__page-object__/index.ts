import type { Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';
/**
 * Класс для реализации логики с главной страницей приложения котиков.
 *
 */
export class MainPage {
  private page: Page;
  public buttonSelector: string

  constructor({
    page,
  }: {
    page: Page;
  }) {
    this.page = page;
    this.buttonSelector = '//button[@type="submit"]'
  }

  async openMainPage() {
    return await test.step('Открываю главную страницу приложения', async () => {
      await this.page.goto('/')
    })
  }

  async inputInSearch(data: string) {
    return await test.step(`Ввожу в строку поиска данные ${data}`, async () => {
      await this.page.fill('//input[@placeholder]', data);
    })
  }
}

export type MainPageFixture = TestFixture<
  MainPage,
  {
    page: Page;
  }
>;

export const mainPageFixture: MainPageFixture = async (
  { page },
  use
) => {
  const mainPage = new MainPage({ page });

  await use(mainPage);
};

