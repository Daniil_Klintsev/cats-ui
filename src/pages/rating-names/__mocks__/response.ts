export const mockApiResponse = {
	status: 500,
	body: JSON.stringify({ message: 'Internal Server Error' }),
};