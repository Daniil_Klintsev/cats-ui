import { test as base, expect } from '@playwright/test';
import { mockApiResponse } from '../__mocks__/response';
import { RatingPage, ratingPageFixture } from '../__page-object__';

const test = base.extend<{ ratingPage: RatingPage }>({
  ratingPage: ratingPageFixture,
});

test('При ошибке сервера в методе raiting - отображается попап ошибки', async ({
  page,
  ratingPage,
}) => {

  await page.route(
    request => request.href.includes('/api/likes/cats/rating'),
    async route => {
      await route.fulfill({
        headers: {
          'Content-Type': 'application/json',
        },
        ...mockApiResponse,
      });
    }
  );

  await ratingPage.openRatingPage();

  await expect(page.locator(ratingPage.errorBlockSelector)).toBeVisible();
});

test('Топ рейтинга лайков отображается в порядке убывания', async ({ page, ratingPage }) => {

  await ratingPage.openRatingPage();

  const likes = await ratingPage.getLikeCounts();
  await ratingPage.checkLikes(likes);
});
